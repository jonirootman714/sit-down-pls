import ToggleContent from "../components/ToggleContent"
import { animDelay } from "../utils/vars"
import Choices from 'choices.js'

const sitySelect =  new Choices('#sity-select', {
  searchEnabled:false,
  itemSelectText: '',
  allowHTML: true,
  classNames: {
    containerOuter: 'sity-select',
    containerInner: 'sity-select__inner',
    input: 'sity-select__input',
    inputCloned: 'sity-select__input--cloned',
    list: 'sity-select__list',
    listItems: 'sity-select__list--multiple',
    listSingle: 'sity-select__list--single',
    listDropdown: 'sity-select__list--dropdown',
    item: 'sity-select__item',
    itemSelectable: 'sity-select__item--selectable',
    itemDisabled: 'sity-select__item--disabled',
    itemChoice: 'sity-select__item--choice',
    placeholder: 'sity-select__placeholder',
    group: 'sity-select__group',
    groupHeading: 'sity-select__heading',
    button: 'sity-select__button',
    activeState: 'is-active',
    focusState: 'is-focused',
    openState: 'is-open',
    disabledState: 'is-disabled',
    highlightedState: 'is-highlighted',
    selectedState: 'is-selected',
    flippedState: 'is-flipped',
    loadingState: 'is-loading',
    noResults: 'has-no-results',
    noChoices: 'has-no-select'
  },
})

const divSity = document.createElement('div')
divSity.innerHTML = `
  <svg class="sity-select__svg" aria-hidden="true" width="12" height="8" viewBox="0 0 12 8">
    <use xlink:href="#select-arrow"></use>
  </svg>
`
document.querySelector('.sity-select').append(divSity.firstElementChild)

const burger = new ToggleContent({
  trigger: document.querySelector('#burger-btn'),
  content: document.querySelector('.burger__content'),
  animDelay,
  activeClassTrigger: 'burger__btn--active',
  handler: () => {
    burger.toggle()
  },
})

const headerCategory = new ToggleContent({
  trigger: document.querySelector('.header__bottom-category-btn'),
  content: document.querySelector('.header__bottom-category-content'),
  animDelay,
  handler: (event) => {
    headerCategory.toggle()
    event._isClick = true;
  },
})

export { burger, headerCategory}