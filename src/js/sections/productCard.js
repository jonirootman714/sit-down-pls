import Tabs from "../components/Tabs";
import ModalWindow from "../components/ModalWindow";
import Swiper, { Navigation } from 'swiper';
import CheckFormIsValid from "../components/CheckFormIsValid";

import '../../assets/svg/elephant.svg'

const productTabs = new Tabs({
  view: document.querySelectorAll('.product-card-main__preview-item'),
  tabs: document.querySelectorAll('.product-card-main__tabs-item'),
  viewActiveClass: 'product-card-main__preview-item--active',
  tabActiveClass: 'product-card-main__tabs-item--active'
})

productTabs.init()

import productImage1 from '../../assets/img/product-card/main/1-big.png'
import productImage2 from '../../assets/img/product-card/main/2-big.png'
import productImage3 from '../../assets/img/product-card/main/3-big.png'
import productImage4 from '../../assets/img/product-card/main/4-big.png'
import productImagePreview1 from '../../assets/img/product-card/main/1-big-active.png'
import productImagePreview2 from '../../assets/img/product-card/main/2-big-active.png'
import productImagePreview3 from '../../assets/img/product-card/main/3-big-active.png'
import productImagePreview4 from '../../assets/img/product-card/main/4-big-active.png'

const sliderModal = new ModalWindow({
  content: `
    <div class="product-card-slider">

      <ul class="list product-card-slider__preview">
        <li class="list__item product-card-slider__preview-item product-card-slider__preview-item--active">
          <img class="img" src="${productImagePreview1}" alt="">
        </li>
        <li class="list__item product-card-slider__preview-item">
          <img class="img" src="${productImagePreview2}" alt="">
        </li>
        <li class="list__item product-card-slider__preview-item">
          <img class="img" src="${productImagePreview3}" alt="">
        </li>
        <li class="list__item product-card-slider__preview-item">
          <img class="img" src="${productImagePreview4}" alt="">
        </li>
      </ul>

      <div class="swiper product-card-slider__swiper">
        <ul class="list product-card-slider__tabs swiper-wrapper">
          <li class="list__item product-card-slider__tabs-item swiper-slide" data-tab="1">
            <img class="img product-card-slider__tabs-item-img" src="${productImage1}" alt="1 tab">
          </li>
          <li class="list__item product-card-slider__tabs-item swiper-slide" data-tab="2">
            <img class="img product-card-slider__tabs-item-img" src="${productImage2}" alt="2 tab">
          </li>
          <li class="list__item product-card-slider__tabs-item swiper-slide" data-tab="3">
            <img class="img product-card-slider__tabs-item-img" src="${productImage3}" alt="3 tab">
          </li>
          <li class="list__item product-card-slider__tabs-item swiper-slide" data-tab="4">
            <img class="img product-card-slider__tabs-item-img" src="${productImage4}" alt="4 tab">
          </li>
        </ul>
        <button class="btn icon product-card-slider__prev">
          <svg class="icon__arrow" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="16" cy="16" r="15" stroke-width="2"></circle>
            <path d="M14 11L19 16L14 21" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
          </svg>
        </button>
        <button class="btn icon product-card-slider__next">
          <svg class="icon__arrow" width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="16" cy="16" r="15" stroke-width="2"></circle>
            <path d="M14 11L19 16L14 21" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
          </svg>
        </button>
      </div>
    </div>
  `,
  boxClass: 'product-card-slider-box'
})

sliderModal.init()

const productCardModalSlider = new Swiper('.product-card-slider__swiper', {
  modules: [Navigation],
  navigation: {
    nextEl: '.product-card-slider__next',
    prevEl: '.product-card-slider__prev',
    disabledClass: 'icon_disabled'
  },
  slidesPerView: 'auto',
  slidesPerGroup: 1,
  spaceBetween: 78,
  breakpoints: {
    0: {
      spaceBetween: 42,
      slidesPerGroup: 1,
    },
    600: {
      slidesPerGroup: 2,
    },
    992: {
      allowTouchMove: false,
    },
    1200: {}
  }
})


const productModalTabs = new Tabs({
  view: document.querySelectorAll('.product-card-slider__preview-item'),
  tabs: document.querySelectorAll('.product-card-slider__tabs-item'),
  viewActiveClass: 'product-card-slider__preview-item--active',
  tabActiveClass: 'product-card-slider__tabs-item--active'
})
productModalTabs.init()

document.querySelectorAll('.product-card-main__preview-item').forEach((el, index) => el.addEventListener('click', () => {
  sliderModal.open()
  productModalTabs.active(index + 1)
}))


const productCardSimilarSlider = new Swiper('.similar__slider', {
  modules: [Navigation],
  navigation: {
    nextEl: '.similar__navigation-next',
    prevEl: '.similar__navigation-prev',
    disabledClass: 'icon_disabled'
  },
  slidesPerView: 'auto',
  slidesPerGroup: 1,
  spaceBetween: 32,
  breakpoints: {
    0: {
      spaceBetween: 16,
    },
    600: {}
  }
});

const formModal = new ModalWindow({
  content: `
    <div class="product-card-form">
      <h2 class="title-res product-card-form__title">Купить в один клик</h2>
      <p class="text product-card-form__desc">
        Чтобы оформить заказ — заполните формы ниже и наш менеджер свяжется с вами в течении часа.
      </p>
      <form class="form product-card-form__form" action="#" method="POST">
        <input class="input product-card-form__input" type="text" placeholder="Как вас зовут?">
        <input class="input product-card-form__input" type="tel" placeholder="Ваш телефон">
        <input id="modal-form-btn" class="btn btn-primary btn-primary_theme_primary product-card-form__submit" type="submit" value="Отправить">
        <div class="product-card-form__policy">
          <label class="checkbox checkbox_theme_defalt">
            <input class="checkbox__input" type="checkbox" checked="checked">
            <span class="checkbox__text product-card-form__checkbox-text">Принимаю</span>
            <span class="checkbox__mark">
              <svg class="checkbox__mark-svg" aria-hidden="true" width="11" height="9" viewBox="0 0 11 9">
                <use xlink:href="#check"></use>
              </svg>
            </span>
          </label>
          <a class="link link-underline product-card-form__agreement" href="#">пользовательское соглашение</a>
        </div>
      </form>
    </div>
  `,
  boxClass: 'product-card-form-box'
})


const thankModal = new ModalWindow({
  content: `
    <div class="product-card-thank">
      <svg class="product-card-thank__svg" aria-hidden="true" width="172" height="117" viewBox="0 0 172 117">
        <use xlink:href="#elephant"></use>
      </svg>
      <p class="text product-card-thank__text">
        Спасибо, мы вам перезвоним!
      </p>
    </div>
  `,
  boxClass: 'product-card-thank-box'
})

formModal.init()
thankModal.init()

document.querySelector('.product-card-main__info-btn').addEventListener('click', (event) => {
  event.preventDefault()
  formModal.open()
})

const formProductCard = new CheckFormIsValid({
  inputs: document.querySelectorAll('.product-card-form__input'),
  inputErrorClass: 'input--error',
  form: document.querySelector('.product-card-form__form'),
  formValidCallback: function () {
    formProductCard.clearInputs()
    formModal.close()
    setTimeout(() => {
      thankModal.open()
    }, 200)
  }
})

formProductCard.init()
