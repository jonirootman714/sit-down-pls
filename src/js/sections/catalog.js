import ToggleContent from "../components/ToggleContent"
import Swiper, { Pagination, Navigation, Grid } from 'swiper';
import { animDelay } from "../utils/vars"

const toggleItems = document.querySelectorAll('.catalog__filter-btn')

const CatalogCategory = new ToggleContent({
  trigger: toggleItems[0],
  content: toggleItems[0].nextElementSibling,
  layer: 15,
  animDelay,
  handler: () => {
    CatalogCategory.toggle()
  },
})
const CatalogCost = new ToggleContent({
  trigger: toggleItems[1],
  content: toggleItems[1].nextElementSibling,
  layer: 15,
  animDelay,
  handler: () => {
    CatalogCost.toggle()
  },
})
const CatalogSell = new ToggleContent({
  trigger: toggleItems[2],
  content: toggleItems[2].nextElementSibling,
  animDelay,
  handler: () => {
    CatalogSell.toggle()
  },
})
const CatalogColor = new ToggleContent({
  trigger: toggleItems[3],
  content: toggleItems[3].nextElementSibling,
  animDelay,
  handler: () => {
    CatalogColor.toggle()
  },
})

const CatalogSlider = new Swiper('.catalog__slider', {
  modules: [Navigation, Grid, Pagination],
  grid: {
    rows: 3,
    fill: 'row'
  },
  slidesPerGroup: 3,
  slidesPerView: 3,
  spaceBetween: 32,
  allowTouchMove: false,
  pagination: {
    el: '.catalog__slider-pagination',
    clickable: true,
    bulletActiveClass: 'catalog__slider-pagination-item--active',
    bulletClass: 'catalog__slider-pagination-item',
    renderBullet: function (index, className) {
      return `<button class="btn btn-primary btn-primary_theme_secondary ${className}">${index + 1}</button>`;
    },
  },
  breakpoints: {
    0: {
      slidesPerGroup: 2,
      slidesPerView: 2,
      grid: {
        rows: 3,
        fill: 'row'
      },
      spaceBetween: 16,
    },
    600: {
      slidesPerGroup: 2,
      slidesPerView: 2,
      grid: {
        rows: 3,
        fill: 'row'
      },
    },
    992: {
      slidesPerGroup: 3,
      slidesPerView: 3,
      grid: {
        rows: 3,
        fill: 'row'
      },
    }
  }
});

const main = document.querySelector('.main')
main.style.maxWidth = `${window.innerWidth}px`

function adaptive() {
  if (window.innerWidth < 1201) {
    CatalogCategory.init()
    CatalogCost.init()
    CatalogSell.init()
    CatalogColor.init()
  } else {
    CatalogCategory.destroy()
    CatalogCost.destroy()
    CatalogSell.destroy()
    CatalogColor.destroy()
  }

  if (window.innerWidth < 992) {
    CatalogSlider.update()

  } else {
  }
  if (window.innerWidth < 320) {
    main.style.maxWidth = `320px`
  } else {
    main.style.maxWidth = `${window.innerWidth}px`
  }
}
adaptive()

window.addEventListener('resize', adaptive)
