import Swiper, { Pagination, Navigation, Autoplay } from 'swiper';
import CheckFormIsValid from '../components/CheckFormIsValid';

const heroSlider = new Swiper('.hero__slider', {
  loop: true,
  autoplay: {
    delay: 4000,
  },

  modules: [Pagination, Autoplay],
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
    bulletActiveClass: 'hero__slider-pagination-item_active',
    bulletClass: 'hero__slider-pagination-item'
  },
});

const specialOfferSlider = new Swiper('.special-offer__slider', {
  modules: [Navigation],
  navigation: {
    nextEl: '.special-offer__navigation-next',
    prevEl: '.special-offer__navigation-prev',
    disabledClass: 'icon_disabled'
  },
  slidesPerView: 'auto',
  slidesPerGroup: 1,
  spaceBetween: 32,
});

const usefulSlider = new Swiper('.useful__slider', {
  modules: [Navigation],
  navigation: {
    nextEl: '.useful__navigation-next',
    prevEl: '.useful__navigation-prev',
    disabledClass: 'icon_disabled'
  },
  slidesPerView: 'auto',
  slidesPerGroup: 1,
  spaceBetween: 32,
});

const highRatindGrid = document.querySelector('.high-rating__grid')

function highRatingAdaptive() {
  if (window.innerWidth >= 1024) {
    highRatindGrid.classList.add('high-rating__grid_overflow_auto')
  } else {
    highRatindGrid.classList.remove('high-rating__grid_overflow_auto')
  }

  if (window.innerWidth < 1023 && window.innerWidth > 601) {
    highRatindGrid.classList.add('high-rating__grid_overflow_768')
  } else {
    highRatindGrid.classList.remove('high-rating__grid_overflow_768')
  }

  if (window.innerWidth < 600 && window.innerWidth > 475) {
    highRatindGrid.classList.add('high-rating__grid_overflow_600')
  } else {
    highRatindGrid.classList.remove('high-rating__grid_overflow_600')
  }

  if (window.innerWidth < 476) {
    highRatindGrid.classList.add('high-rating__grid_overflow_320')
  } else {
    highRatindGrid.classList.remove('high-rating__grid_overflow_320')
  }
}
highRatingAdaptive()
window.addEventListener('resize', highRatingAdaptive)

document.querySelector('.high-rating__btn').addEventListener('click', function() {
  this.parentNode.remove()
  highRatindGrid.classList.remove('high-rating__grid_overflow_320')
  highRatindGrid.classList.remove('high-rating__grid_overflow_600')
  highRatindGrid.classList.remove('high-rating__grid_overflow_768')
  highRatindGrid.classList.remove('high-rating__grid_overflow_auto')
  window.removeEventListener('resize', highRatingAdaptive)
})

const formFeedback = new CheckFormIsValid({
  inputs: document.querySelectorAll('.feedback__form-input'),
  inputErrorClass: 'input--error',
  form: document.querySelector('.feedback__form'),
  formValidCallback: function() {
    formFeedback.clearInputs()
  }
})

formFeedback.init()