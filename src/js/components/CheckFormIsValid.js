export default class CheckFormIsValid {
  constructor(options) {
    this.inputs = options.inputs
    this.inputErrorClass = options.inputErrorClass
    this.form = options.form
    this.formValidCallback = options.formValidCallback ? options.formValidCallback : function () {}
  }

  init() {
    const inputs = this.inputs
    const formValidCallback = this.formValidCallback

    inputs.forEach(el => {
      createLog(el)
      el.addEventListener('input', function () {
        checkInputIsEmpty(el, el.previousSibling)
      })
    })

    this.form.addEventListener('submit', submitForm)

    function submitForm(event) {
      event.preventDefault()
      let isValid

      inputs.forEach(el => {
        isValid = checkInputIsEmpty(el, el.previousSibling)
      })

      if (isValid) {
        formValidCallback()
      }
    }

    function createLog(input) {
      const log = document.createElement('div')
      log.classList.add('input-log')
      log.style.display = 'none'
      input.before(log)
    }

    function checkInputIsEmpty(input, log) {
      if (input.value === '') {
        log.textContent = 'Error: empty stryng'
        log.style.display = 'block'
        return false
      } else {
        log.style.display = 'none'
        return true
      }
    }
  }

  clearInputs() {
    this.inputs.forEach(el => {
      el.value = ''
    })
  }
}
