export default class ToggleContent {
  constructor(options) {
    this.trigger = options.trigger
    this.content = options.content
    this.handler = options.handler
    this.animDelay = options.animDelay
    this.activeClassTrigger = options.activeClassTrigger ? options.activeClassTrigger : 'toggle__btn--active'
    this.activeClassContent = options.activeClassContent ? options.activeClassContent : 'toggle__content--active'
    this.layer = options.layer ? options.layer : false
  }

  isActive = false
  isInit = false

  init() {
    this.trigger.addEventListener('click', this.handler)
    this.trigger.classList.add('toggle__btn')
    this.content.classList.add('toggle__content')
    this.isInit = true
    this.trigger.offsetParent ? this.trigger.offsetParent.style.zIndex = this.layer : console.log(this.trigger)
  }

  destroy() {
    this.trigger.removeEventListener('click', this.handler)
    this.trigger.classList.remove('toggle__btn')
    this.content.classList.remove('toggle__content')
    this.isInit = false
  }

  open() {
    if (!this.isInit) return
    this.trigger.classList.add(this.activeClassTrigger)
    this.content.classList.add(this.activeClassContent)
    setTimeout(() => {
      this.isActive = true
    }, this.animDelay);
  }

  close() {
    if (!this.isInit) return
    this.trigger.classList.remove(this.activeClassTrigger)
    this.content.classList.remove(this.activeClassContent)
    setTimeout(() => {
      this.isActive = false
    }, this.animDelay);
  }

  toggle() {
    if (!this.isInit) return
    if (!this.isActive) this.open()
    if (this.isActive) this.close()
  }
}

