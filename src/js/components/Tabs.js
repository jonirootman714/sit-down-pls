export default class Tabs {
  constructor(options) {
    this.view = options.view
    this.tabs = options.tabs
    this.viewActiveClass = options.viewActiveClass
    this.tabActiveClass = options.tabActiveClass
    this.startActive = options.startActive ? options.startActive: 1
  }

  isActive = false

  init() {
    this.isActive = true
    this.active(this.startActive)
    this.tabs.forEach(el => {
      el.addEventListener('click', () => {
        this.active(+el.dataset.tab)
      })
    })
  }

  active(viewNumber) {
    if (!this.isActive) return
    this.clearActive()
    this.view.forEach((el, index) => {
      if (viewNumber - 1 === index) el.classList.add(this.viewActiveClass)
    })
  }

  clearActive() {
    if (!this.isActive) return
    this.view.forEach(el => el.classList.remove(this.viewActiveClass))
  }
}