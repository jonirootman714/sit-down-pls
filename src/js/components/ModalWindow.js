import '../../assets/svg/close.svg'
import { animDelay } from '../utils/vars'
export default class ModalWindow {
  constructor(options) {
    this.content = options.content
    this.widthModal = options.widthModal
    this.heightModal = options.heightModal
    this.boxClass= options.boxClass ? options.boxClass : false
    this.animDelay = options.animDelay ? options.animDelay : animDelay
    this.closeHandler = options.closeHandler ? options.closeHandler : () => this.close()
    this.closeHandlerOverlay = options.closeHandlerOverlay ? options.closeHandlerOverlay : (event) => {
      if (event.target.classList.contains('modal__overlay')) this.close()
      if (event.target.classList.contains('container')) this.close()
    }
  }

  modalWrapp = document.createElement('div')
  modalOverlay = document.createElement('div')
  modalBox = document.createElement('div')
  modalCloseBtn = document.createElement('button')
  modalContainer = document.createElement('div')

  init() {
    this.modalWrapp.classList.add('modal')
    this.modalWrapp.classList.add('modal--hide')
    this.modalOverlay.classList.add('modal__overlay')
    this.modalBox.classList.add('modal__box')
    if (this.boxClass) {
      this.modalBox.classList.add(this.boxClass )
    }
    this.modalContainer.classList.add('container')

    function closeBtnInit(closeBtn, handler) {
      closeBtn.classList.add('modal__close')
      closeBtn.classList.add('btn')
      closeBtn.innerHTML = `
        <svg class="modal__close-svg" aria-hidden="true" width="20" height="20" viewBox="0 0 20 20">
          <use xlink:href="#close"></use>
        </svg>
      `
      closeBtn.addEventListener('click', handler)
    }

    this.modalBox.style.minWidth = 320 + 'px'

    closeBtnInit(this.modalCloseBtn, this.closeHandler)
    // boxInit(this.modalBox, this.widthModal, this.heightModal)

    this.modalContainer.appendChild(this.modalBox)
    this.modalOverlay.appendChild(this.modalContainer)
    this.modalWrapp.appendChild(this.modalOverlay)

    this.modalOverlay.addEventListener('click', this.closeHandlerOverlay)

    this.modalBox.innerHTML = this.content
    this.modalBox.appendChild(this.modalCloseBtn)

    document.body.appendChild(this.modalWrapp)
  }

  open() {
    this.modalWrapp.classList.remove('modal--hide')
    this.modalOverlay.classList.add('modal__overlay--open')
    this.modalBox.classList.add('modal__box--open')
    document.body.style.overflow = 'hidden'
  }

  close() {
    this.modalOverlay.classList.remove('modal__overlay--open')
    this.modalOverlay.classList.add('modal__overlay--close')
    this.modalBox.classList.remove('modal__box--open')
    setTimeout(() => {
      document.body.style.overflow = 'visible'
      this.modalWrapp.classList.add('modal--hide')
      this.modalOverlay.classList.remove('modal__overlay--close')
    }, animDelay)
  }
}