import './assets/svg/phone.svg'
import './assets/svg/select-arrow.svg'
import './assets/svg/cart.svg'
import './assets/svg/user.svg'
import './assets/svg/search-glass.svg'
import './assets/svg/discount.svg'
import './assets/svg/star.svg'
import './assets/svg/check.svg'
import './assets/svg/tooltip.svg'

import './assets/svg/socials/fb.svg'
import './assets/svg/socials/vk.svg'
import './assets/svg/socials/instagram.svg'

import { burger , headerCategory} from './js/sections/header'
import { main } from './js/utils/vars'

headerCategory.init()


document.querySelector('.header__bottom-category-content').addEventListener('click', (event) => {
  event._isClick = true
})

document.addEventListener('click', (event) => {
  if (event._isClick) return
  headerCategory.close()
})

const categoryLinks = document.querySelectorAll('.header__bottom-nav-link')
categoryLinks.forEach(el => {
  el.addEventListener('click', () => {
    headerCategory.close()
  })
})


main.style.maxWidth = `${window.innerWidth}px`

function adaptive() {
  if (window.innerWidth < 992) {
    burger.init()
  }
  if (window.innerWidth < 320) {
    main.style.maxWidth = `320px`
  } else {
    main.style.maxWidth = `${window.innerWidth}px`
  }
}
adaptive()

window.addEventListener('resize', adaptive)



