<div align="center">

![logo](docs/LOGO.svg "logo")

# **SitSownPls**

[🐘Предварительный просмотр](https://sitdownpls-9892a.web.app/)

</div>

Это многостраничный сайт который является вымышленной компанией «SitDownPls» по продаже различной мебели. На сайте доступны 3 страницы - главная, каталог, и подробное описание товара. При создании сайта использовал методологию БЭМ а также предпроцессор SASS. В качестве сборщика использовался Webpack.

<div style="display: flex; justify-content: space-between;">
  <a href="https://sitdownpls-9892a.web.app/">
    <img src="docs/sdp-main.png" alt="https://sitdownpls-9892a.web.app/" width="350"/>
  </a>
  <a href="https://sitdownpls-9892a.web.app/catalog.html">
    <img src="docs/sdp-catalog.png" alt="https://sitdownpls-9892a.web.app/catalog.html" width="350"/>
  </a>
  <a href="https://sitdownpls-9892a.web.app/productCard.html">
    <img src="docs/sdp-product.png" alt="https://sitdownpls-9892a.web.app/productCard.html" width="350"/>
  </a>
</div>

## Запуск проекта

```bash
npm i
```

- **Production**:

```bash
npm run build
```

- **Development** `(with dev server)`:

```bash
npm run start
```

- **Development** `(without dev server)`:

```bash
npm run dev
```

- **Analyze build**:

```bash
npm run analyzer
```

## Links

### Navigate on site

- main page <http://localhost:8081/index.html>
- catalog <http://localhost:8081/catalog.html>
- product card <http://localhost:8081/productCard.html>

### Libs in project

- [Swiper](https://swiperjs.com/)
- [Choices](https://github.com/Choices-js/Choices)
- [UiRange](https://github.com/yairEO/ui-range)

---

Figma Layout: <https://www.figma.com/file/lBMYXRH6CHIoiIK6xrcibD/sdp.ru?node-id=0%3A1&t=eRm0hxWqETCBuQ6l-0>
