const path = require('path')
const PugPlugin = require('pug-plugin')
const PATHS = {
  src: path.join(__dirname, '../src'),
  dist: path.join(__dirname, '../dist'),
  assets: path.join(__dirname, '../src/assets'),
}

module.exports = {
  entry: {
    index: './src/views/index.pug',
    catalog: './src/views/catalog.pug',
    productCard: './src/views/product-card.pug',
  },
  output: {
    path: PATHS.dist,
    publicPath: '/',
    clean: true,
    filename: 'assets/js/[name].[contenthash:8].js'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          name: 'vendors',
          test: /node_modules/,
          chunks: 'all',
          enforce: true
        }
      }
    },
  },
  resolve: {
    alias: {
      '@img': `${PATHS.assets}/img`,
      '@fonts': `${PATHS.assets}/fonts`
    }
  },
  externals: {
    paths: PATHS
  },
  plugins: [
    new PugPlugin({
      extractCss: {
        filename: 'assets/css/[name].[contenthash:8].css'
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.pug$/,
        loader: PugPlugin.loader
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(css|sass|scss)$/,
        use: [
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  [ 'autoprefixer' ],
                  [ 'css-mqpacker' ],
                  [ 'cssnano' ]
                ],
              },
            },
          },
           'sass-loader'
        ]
      },
      {
        test: /\.(png|jpg|jpeg|ico)/,
        type: 'asset/resource',
        generator: {
          filename: 'assets/img/[name].[hash:8][ext]'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'assets/fonts/[name][ext][query]'
        }
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-sprite-loader',
            options: {
              runtimeCompat: true
            }
          }
        ]
      },
    ]
  },
};