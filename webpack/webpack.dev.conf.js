// --mode development
const webpack = require('webpack')
const { merge } = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf')

const devWebpackConfig = merge(baseWebpackConfig, {
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: {
    static: {
      directory: baseWebpackConfig.externals.paths.dist
    },
    port: 8081,
    hot: false,
    compress: true,
    // clientLogLevel: 'silent'
  },
  plugins: [
    new webpack.SourceMapDevToolPlugin({
      filename: '[file].map',
      exclude: ['vendor.js'],
    })
  ],
})

module.exports = new Promise((resolve) => {
  resolve(devWebpackConfig)
})