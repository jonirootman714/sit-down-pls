// --mode production
const { merge } = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf')
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");

const buildWebpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  plugins: [],
  optimization: {
      minimizer: [
        new ImageMinimizerPlugin({
          minimizer: {
            implementation: ImageMinimizerPlugin.imageminMinify,
            options: {
              plugins: [
                ["jpegtran", { progressive: true }],
                ["optipng", { optimizationLevel: 5 }],
              ],
            },
          },
        }),
      ],
    },
  devtool: false,
  performance: {
    hints: 'warning',
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  }
})

module.exports = new Promise((resolve, reject) => {
  resolve(buildWebpackConfig)
})