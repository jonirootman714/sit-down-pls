// --mode production
const { merge } = require('webpack-merge');
const buildWebpackConfig123 = require('./webpack.base.conf')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const analyzerWebpackConfig = merge(buildWebpackConfig123, {
  mode: 'production',
  plugins: [
    new BundleAnalyzerPlugin()
  ],
  devtool: false,
  performance: {
    hints: 'warning',
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  }
})

module.exports = new Promise((resolve, reject) => {
  resolve(analyzerWebpackConfig)
})